# Project setup

Follow these steps to get started with the project on your local environment.

Before proceeding, please make sure you have [watbox] installed.

## Get your composer credentials

During installation, you will be asked for credentials to repo.magento.com. 
Customer-specific credentials can be found in [passbolt]. 
Ask your project lead to share the composer credentials with you.

If no credentials are available, you can use the globally available credentials. 
**Be aware that any packages purchased on magento marketplace will not be available without the client-specific credentials.**

The following command will print the username and password separated by a comma.
```shell
ssh api@iec.vaimo.com get-magento-repo-keys magento
```

## Import the project 

```shell
$ wat import clean-m2-ee

Import project clean-m2-ee
Destination directory: clean-m2-ee
Cloning repository from git@bitbucket.org:vaimo/project_clean-m2-ee.git
Creating project file
Done.
Creating new .env file based on your project packages. To re-use the .env file, save a copy as `.warden.env.dist`
Writing environment config for `clean-m2-ee` (magento 2.3.6)
Pulling elasticsearch ... done
Pulling rabbitmq      ... done
Pulling redis         ... done
Pulling db            ... done
Pulling php-fpm       ... done
Pulling nginx         ... done
Pulling varnish       ... done
Pulling php-debug     ... done
Connecting traefik to clean-m2-ee_default network
Connecting tunnel to clean-m2-ee_default network
Connecting mailhog to clean-m2-ee_default network
Docker Compose is now in the Docker CLI, try `docker compose up`

clean-m2-ee_rabbitmq_1 is up-to-date
clean-m2-ee_elasticsearch_1 is up-to-date
clean-m2-ee_db_1 is up-to-date
clean-m2-ee_redis_1 is up-to-date
clean-m2-ee_php-fpm_1 is up-to-date
clean-m2-ee_php-debug_1 is up-to-date
clean-m2-ee_nginx_1 is up-to-date
clean-m2-ee_varnish_1 is up-to-date
This project contains a version of magento/product-enterprise-edition greater than 2.3.5 which has not yet been tested with this version of Aja. Please report any issues to our Internal Service Desk at https://itsupport.vaimo.com
This project contains a version of magento/product-community-edition greater than 2.3.5 which has not yet been tested with this version of Aja. Please report any issues to our Internal Service Desk at https://itsupport.vaimo.com
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Package operations: 559 installs, 0 updates, 0 removals
  - Installing magento/module-ui (101.1.6): Loading from cache
  - Installing magento/module-media-storage (100.3.6): Loading from cache
  - Installing magento/module-variable (100.3.4):
    Authentication required (repo.magento.com):
```

## Answer yes to replacing the database (it's empty).

```shell
Writing patch info to install file
A database named magento already exists, do you want to replace it? [y/N] y
[Progress: 1317 / 1317]
Magento installation complete.
Magento Admin URI: /admin
Enabled developer mode
Main url https://app.clean-m2-ee.test/
Admin url https://app.clean-m2-ee.test/admin
User: admin
Password: test123
Site url: https://app.clean-m2-ee.test/
Executing after_project_install_cmd hook...
```

## Configure connections to varnish, elasticsearch, redis, etc.

Watbox asks you if this should be done automatically during setup.

```shell
Do you want to configure Magento to use watbox services? [Y/n]
Detecting Magento configuration features. This will take a little while.
> bin/magento setup:config:set --cache-backend=redis --cache-backend-redis-server=redis --cache-backend-redis-port=6379 --cache-backend-redis-db=0 --page-cache=redis --page-cache-redis-server=redis --page-cache-redis-port=6379 --page-cache-redis-db=1 --session-save=redis --session-save-redis-host=redis --session-save-redis-port=6379 --session-save-redis-log-level=4 --session-save-redis-db=2 --http-cache-hosts=varnish:80 --amqp-host=rabbitmq --amqp-port=5672 --amqp-user=guest --amqp-password=guest
Overwrite the existing configuration for db-ssl-verify?[Y/n]
Overwrite the existing configuration for session-save?[Y/n]
We saved default values for these options: amqp-port, amqp-virtualhost, amqp-ssl, amqp-ssl-options, consumers-wait-for-messages, db-ssl-key, db-ssl-cert, db-ssl-ca, db-ssl-verify.
> bin/magento config:set --lock-env catalog/search/engine elasticsearch7
Value was saved in app/etc/env.php and locked.
> bin/magento config:set --lock-env catalog/search/elasticsearch7_server_hostname elasticsearch
Value was saved in app/etc/env.php and locked.
> bin/magento config:set --lock-env catalog/search/elasticsearch7_server_port 9200
Value was saved in app/etc/env.php and locked.
> bin/magento config:set --lock-env system/full_page_cache/caching_application 2
Value was saved in app/etc/env.php and locked.
```

If you need to run the configuration again at a later time, run `warden shell` followed by `m2-autoconfig --all`.

## Self-sign a SSL certificate

```shell
$ warden sign-certificate clean-m2-ee.test

==> Generating private key clean-m2-ee.test.key.pem
Generating RSA private key, 2048 bit long modulus
.....................+++
........................................................................................+++
e is 65537 (0x10001)
==> Generating signing req clean-m2-ee.test.crt.pem
==> Generating certificate clean-m2-ee.test.crt.pem
Signature ok
subject=/C=US/O=Warden.dev/CN=clean-m2-ee.test
Getting CA Private Key
==> Updating traefik
Docker Compose is now in the Docker CLI, try `docker compose up`

traefik is up-to-date
Connecting traefik to clean-m2-ee-m24_default network
Connecting tunnel to clean-m2-ee-m24_default network
Connecting mailhog to clean-m2-ee-m24_default network
Connecting traefik to clean-m2-ee_default network
Connecting tunnel to clean-m2-ee_default network
Connecting mailhog to clean-m2-ee_default network
Restarting traefik ... done
```

# That's it 🎉 

- The frontend is now visible at https://app.clean-m2-ee.test/
- Use [MAGlogin] bookmarks to access the administration panel
- Use `warden shell` to connect to the PHP container
- Use `warden debug` to connect to the xdebug container
- Refer to [watbox]' and [warden]'s documentation for more details

[watbox]: https://backstage.vaimo.network/docs/default/Component/watbox
[passbolt]: https://passbolt.vaimo.network/
[MAGlogin]: https://confluence.vaimo.com/display/SECURITY/Maglogin
[warden]: https://docs.warden.dev/
