# Clean project for Magento 2, Enterprise Edition

[![build status](https://ci.vaimo.com/job/project_clean_m2_ee-demo/badge/icon)](https://ci.vaimo.com/job/project_clean_m2_ee-demo)

This repository contains the base/template project for Vaimo M2 EE projects.

## Installation

```
#!shell

aja project import clean-m2-ee
aja project import-db --from demo

```

## Creating project-specific package/module

The setup of this project discourages adding project-specific modules directly to app/code due to that making
it harder to publish those modules as public repositories in the future (extracting project-specific modules).

The approach suggested by the setup is to add your modules under <project>/modules and require them with:

    composer require <package-name>:dev-default

... where `<package-name>` is taken from `<project root>/modules/my-package/composer.json`

This will make the module accessible via vendor/vaimo the exact same way you are able to access 
published composer packages.

Other reasons for discouraging dumping project specific code in app/code:

1. 3rd party package dependencies not clearly declared (due to not having composer.json).
1. continuous validation that package is properly configured (in case the developer is already defining
   a composer.json for every module in app/code, which some have been doing).
1. one access point for every project component (everything is accessible via vendor).

HINT: Use vaimo/skeleton-module-magento as a basis for creating your own module to get a list of fully
      working features included with the created module without extra work. More information available
      in the repository of before-mentioned Skeleton package.

## Adding a project-specific patch

The project has been pre-configured to expect patches to be added to the following folder:

    <project>/patches
    
The recommended file structure is the following (example based on magento/module-catalog): 

    <project>/patches/Magento_Catalog/description-for-patch/version-100.0.0.patch

That format makes it very clear to users on how to version-branch a patch, etc.

1. The file paths of the patch should be relevant to the root of magento/module-catalog
   https://github.com/vaimo/composer-patches#basic-usage-patch-file-format

2. Configure the package targeting info of the patch by following docs here:
    https://github.com/vaimo/composer-patches#patches-patch-declaration-with-embedded-target-information
    
3. The recommended minimal tags are: @package @version @ticket (or @link) + description before the tags
